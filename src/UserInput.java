package Paket;

import java.util.ArrayList;
import java.util.List;

public class UserInput {

    public abstract static class TextInput { //Mora da bude static da bi mogla da se nasledi? Zasto ne moze samo abstract?
        List<Character> lista = new ArrayList<>();
        public abstract void add(char c); //Ne moze da bude static ako se override neka metoda?
        public String getValue() {
            List<String> novaLista= new ArrayList<>();
            for (Character c : lista) {
                novaLista.add(String.valueOf(c));
            }
            return String.join("", novaLista);
        }
    }
    public static class NumericInput extends TextInput{ //Mora da bude static inace ne moze da se napravi instanca?
        @Override
        public void add (char c){
        if (c>='0' && c<='9'){
            lista.add(c);
            //System.out.println("Dodat je broj");
            }
            //System.out.println("Nije broj");
        }

    }
    public static void main (String [] args ){
        TextInput input= new NumericInput();
        input.add('a');
        input.add('5');
        input.add('s');
        input.add('1');
        input.add('7');
        System.out.println(input.getValue());
    }
}
